import pytest
from team_henri_allan_hugo.account import Account, statement_to_str
from datetime import date 



@pytest.fixture
def empty_account():
    return Account()

@pytest.fixture
def deposited_account():
    my_account = Account()
    my_account.deposit(1000, date.today())
    return my_account

def test_create_account_is_balance_empty(empty_account):
    assert empty_account.balance == 0

def test_deposit_is_balance_correct(empty_account):
    empty_account.deposit(1000, date.today())
    assert empty_account.balance == 1000

def test_print_deposit_operation_is_correct(empty_account):
    empty_account.deposit(1000, date.today())
    assert statement_to_str(empty_account.statement()) == f"""date || credit || debit || balance
{date.today()} || 1000.00 || || 1000.00"""

def test_withdrawal_is_balance_correct(deposited_account):
    deposited_account.withdrawal(500, date.today())
    assert deposited_account.balance == 500

def test_print_withdrawal_operation_is_correct(deposited_account):
    deposited_account.withdrawal(500, date.today())
    assert statement_to_str(deposited_account.statement()) == f"""date || credit || debit || balance
{date.today()} || || 500.00 || 500.00
{date.today()} || 1000.00 || || 1000.00"""

def test_deposit_operation_is_correct(empty_account):
    today = date.today()
    empty_account.deposit(1000, today)
    assert empty_account.statement()[0] == {'date': today, 'credit': 1000, 'debit': None, 'balance': 1000}

def test_withdrawal_operation_is_correct(deposited_account):
    today = date.today()
    deposited_account.withdrawal(500, today)
    assert deposited_account.statement()[1] == {'date': today, 'credit': None, 'debit': 500, 'balance': 500}