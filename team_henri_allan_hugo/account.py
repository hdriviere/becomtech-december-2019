from datetime import date
import copy

class Account:

    def __init__(self):
        self.operations = []

    @property
    def balance(self):
        if not self.operations:
            return 0
        return self.operations[-1]['balance']

    def deposit(self, value: float, date):
        new_balance = self.balance + float(value)
        deposit_operation = {'date': date, 'credit': float(value), 'debit': None, 'balance': new_balance}
        self.operations.append(deposit_operation)

    def withdrawal(self, value: float, date):
        new_balance = self.balance - float(value)
        withdrawal_operation = {'date': date, 'credit': None, 'debit': float(value), 'balance': new_balance}
        self.operations.append(withdrawal_operation)

    def statement(self):
        return copy.deepcopy(self.operations)

def statement_to_str(statement):
    statement_str = "date || credit || debit || balance"
    for op in reversed(statement):
        credit = f"{op['credit']:.2f} " if op['credit'] else ""
        debit = f"{op['debit']:.2f} " if op['debit'] else ""
        balance = f"{op['balance']:.2f}" if op['balance'] else ""
        statement_str += f"\n{op['date']} || {credit}|| {debit}|| {balance}"
    return statement_str
