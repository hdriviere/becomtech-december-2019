from account import Account
import datetime

if __name__ == "__main__":
    my_account = Account()
    my_account.deposit(1000, datetime.datetime(2012, 1, 10))
    my_account.deposit(2000, datetime.datetime(2012, 1, 13))
    my_account.withdrawal(500, datetime.datetime(2012, 1, 14))
    print(my_account.statement_to_str())